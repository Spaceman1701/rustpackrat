# Packrat Parsing for Rust

This is a library that supports the implementation of Packrat parsers


# Goals

* Packrat cache implementation
  - Left recursion (direct and indirect)
* Readable API
* Non-perscriptive

# Non-Goals

* Being a parser generator
